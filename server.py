from virustotal3.core import get_analysis
from requests import request
import socket
from vt_private_api_key import API_KEY # file hidden with .gitignore

class VirusTotal:

    def __init__(self) -> None:
        self.scannedProcessesCount = 0

    """
    -- send file to scanVirusTotal --
    * input: self, file's content
    * return: file's scan ID
    """
    def sendFileToScan(self, fileData):
        
        files = { 'file': (f"file{self.scannedProcessesCount}.exe", fileData) }
        url = "https://www.virustotal.com/api/v3/files"
        headers = { "x-apikey": API_KEY }

        response = request('POST', url, files=files, headers=headers).json()

        self.scannedProcessesCount += 1
        return response['data']['id']
    
    """
    -- get file scaned result from VirusTotal --
    * input: self, file's scan ID
    * return: json VirusTotal API response
    """
    def getFileScan(self, scanID):
        return get_analysis(API_KEY, scanID)['data']['attributes']['stats']


HOST = '127.0.0.1'
PORT = 42069

def main():

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen()
            print(f"Listening on port {PORT}...")
                        
            while True:
                con, addr = s.accept()

                # receive file 
                fileSize = int(con.recv(10).decode())
                fileData = con.recv(fileSize).decode('iso-8859-1')

                virusTotal = VirusTotal()
                data = ""
                
                try:
                    # send file to test and get file result
                    fileScanID = virusTotal.sendFileToScan(fileData)
                    response = virusTotal.getFileScan(fileScanID)
                    data = str(response['suspicious'] + response['malicious']).encode()
                except Exception as e:
                    data = ("error: " + repr(e)).encode()
                
                # send result to client
                dataLength = str(len(data)).zfill(10)
                con.sendall(dataLength.encode())
                con.sendall(data)

    except Exception as e:
        print(e)

if __name__ == '__main__': 
    main()