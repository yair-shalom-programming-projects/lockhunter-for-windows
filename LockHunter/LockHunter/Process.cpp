#include "Process.h"
#include <iostream>
#include "WinapiFunctionFailedException.h"
#include <winternl.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <sstream>


static u64 lpfiletimeToull(const FILETIME& pTime) noexcept
{
	return ((u64)pTime.dwHighDateTime << 32) | pTime.dwLowDateTime;
}



Process::TimesInLands Process::getTimeInLands() const
{
	FILETIME exitTime, creationTime, kernelTime, userTime;

	// note - creationTime & exitTime are unused
	if (!GetProcessTimes(_handle, &creationTime, &exitTime, &kernelTime, &userTime))
		throw WinapiFunctionFailedException("GetProcessTimes");

	return { lpfiletimeToull(kernelTime), lpfiletimeToull(userTime) };
}



Process::Process(const RM_PROCESS_INFO& processInfo) :
	_id(processInfo.Process.dwProcessId), _parentID(), _name(processInfo.strAppName),
	_type(TYPES.at(processInfo.ApplicationType))
{

	if (!(_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, _id)))
		throw WinapiFunctionFailedException("OpenProcess");

	findPath();
	findParentID();
	findCreationDate(processInfo);

}


Process::~Process()
{
}



Process& Process::operator=(const Process& other)
{

	_handle = other._handle;
	_creationDate = other._creationDate;
	_id = other._id;
	_VTL = other._VTL;
	_type = other._type;
	_name = other._name;
	_path = other._path;
	_parentID = other._parentID;

	return *this;

}


std::wostream& operator<<(std::wostream& os, const Process& other)
{

	auto [kernelSpaceTime, userSpaceTime] = other.getTimeInLands();

	return os  
		
		<< "Name: "				    << other._name			    << '\n'
		<< "Type: "					<< other._type				<< '\n'
		<< "PID: "				    << other._id				<< '\n'
		<< "Creation Date: "        << other._creationDate		<< '\n'
		<< "Time In Kernel Space: " << kernelSpaceTime  		<< "ns\n"
		<< "Time In User Space: "   << userSpaceTime 		    << "ns\n"
		<< "Path: "				    << other._path				<< '\n'
		<< "Parent Process PID: "  << other._parentID			<< '\n';

}




void Process::findParentID()
{

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 procentry;
	procentry.dwSize = sizeof(PROCESSENTRY32);
	
	// loop process
	if (Process32First(hSnapShot, &procentry))
	{
		do
		{
			if (_id == procentry.th32ProcessID)
			{
				_parentID = procentry.th32ParentProcessID;
				return;
			}

		}
		while (Process32Next(hSnapShot, &procentry));
	}

	_parentID = 0;

}

void Process::findCreationDate(const RM_PROCESS_INFO& info)
{

	SYSTEMTIME time;
	
	if (!FileTimeToSystemTime(&info.Process.ProcessStartTime, &time))
		throw WinapiFunctionFailedException("FileTimeToSystemTime");
	
	std::wstringstream os;
	os << time.wHour + 2 << ":" << time.wMinute << ":" << time.wSecond << ":" << time.wMilliseconds;

	_creationDate = os.str();

}

void Process::findPath()
{

	DWORD pathSize = MAX_PATH;
	WCHAR path[MAX_PATH];

	if (!QueryFullProcessImageNameW(_handle, 0, path, &pathSize) || pathSize > MAX_PATH)
		throw WinapiFunctionFailedException("QueryFullProcessImageNameW");
	
	_path = path;

}

