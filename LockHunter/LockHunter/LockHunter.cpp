#include "VirusTotal.h"
#include "LockHunter.h"
#include "WinapiFunctionFailedException.h"
#include <windows.h>
#include <RestartManager.h>
#include <iostream>
#include <functional>
#include <array>
#include "RestartManager.h"


using namespace std::string_literals;
using std::wcout;
using std::cin;
using std::endl;



//? Constructor ?//


/* -- C'tor -- */
LockHunter::LockHunter(const std::wstring& filePath) :
    _filePath(filePath.c_str())
{
	findProcessesLockingFile();
}


//? Methods ?//

void LockHunter::findProcessesLockingFile()
{
    
    RestartManager().findProcessesLockingFile(_filePath, _processes);
    
    if (!_processes.size())
        throw std::exception("no processes locking file!");

}


/* -- handle LockHunter menu -- */
void LockHunter::menu()
{

    const std::array<std::function<bool(LockHunter*)>, 4> OPTIONS = {
		&LockHunter::deleteFile, 
        &LockHunter::unlockFile, 
        &LockHunter::printProcesses,
        &LockHunter::scanForVirus
	};

	Option option = Option::INVALID;
    bool loadMenu = true;

    while (loadMenu)
    {
        do
	    {
	
		    system("cls");
		
		    wcout << "File \"" << _filePath << "\"\n"
			      << _processes.size() << " Locking processes\n";
		
		    for (const auto& process : _processes)
			    wcout << " - " << process.getName() << '\n';
		    
		    wcout << "\nOPTIONS\n"\
			      << " [1] Delete file\n"
			      << " [2] Unlock file\n"
			      << " [3] Get more info about locking processes\n"
			      << " [4] Scan locking processes for viruses (VirusTotal)\n"
			      << " [5] Exit" << endl
		          << "$ ";

		    cin >> option;

	    } while (option == Option::INVALID);


	    if (option == Option::EXIT) return;

        system("cls");
        loadMenu = OPTIONS[(int)option - 1](this);
        system("pause");
    }


}

/*
    -- delete the locked file by unlocking it and then deleting it --
    * output: load menu after this option
*/
bool LockHunter::deleteFile()
{
    unlockFile();

    if (!DeleteFileW(_filePath))
        throw WinapiFunctionFailedException("DeleteFileW");
    
    std::cout << "File deleted successfully\n" << endl;
    return false;
}

/*
    -- unlocking the locked file by closing all locking _processes --
    * output: load menu after this option
*/
bool LockHunter::unlockFile()
{

    HANDLE hProcess = NULL;

    for (const auto& process : _processes)
    {
        if(!(hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, process.getID())))
            throw WinapiFunctionFailedException("OpenProcess");
        
        if (!TerminateProcess(hProcess, 1))
            throw WinapiFunctionFailedException("TerminateProcess");
    }

    wcout << "File unlocked successfully" << endl;
    return false;
}


/* -- print all _processes information -- */
bool LockHunter::printProcesses() const
{
    
    wcout << "Processes locking file \"" << _filePath << "\"\n";
    
    for (const auto& process : _processes)
        wcout << '\n' << process << '\n';
    wcout << endl;

    return true;

}


/* -- scan all _processes for viruses with VirusTotal -- */
bool LockHunter::scanForVirus() const
{
    try
    {
        VirusTotal virusTotal;

        wcout << "SCANS RESULTS\n";
        for (const auto& process : _processes)
        {
            wcout << " * \"" << process.getName() << "\" - ";
            
            auto [success, result] = virusTotal.scanFile(process.getPath());
            
            wcout << "VirusTotal ";

            if (!success)
                throw std::exception(result.c_str());
                
            wcout << "found " << result.c_str() << " suspicious/malicious viruses\n";
        }
    }
    catch (const std::exception& e)
    {
        system("cls");
        wcout << e.what() << endl;
    }

    wcout << endl;

    return true;
}



