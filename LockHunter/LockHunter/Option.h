#pragma once
#include <iostream>


enum class Option
{
	INVALID,
	DELETE_FILE,
	UNLOCK_FILE, 
	SHOW_PROCESSES_INFO, 
	SCAN_PROCESSES_FOR_VIRUS, 
	EXIT
};

std::istream& operator>>(std::istream& is, Option& option);