#include "Client.h"
#include "SocketException.h"
#include "Ws2tcpip.h"
#include <sstream>
#include <iomanip>

//? Constructors ?//

/* -- c'tor -- */
Client::Client(const std::wstring_view ip, const u_short& port) :
	IP(ip.data()), PORT(port), _socket(0)
{
	createSocket();
	connectServer();
}

/* -- d'tor -- */
Client::~Client() noexcept
{
	try {
		closesocket(_socket);
	} catch (...) {
	}
}


//? Methods ?//

/* 
	-- send data to server --
	* input: data to send to server
*/
void Client::send(const std::vector<char>& data)
{

	std::stringstream dataSize;
	dataSize << std::setw(10) << std::setfill('0') << data.size();

	// send data's size (first 10 bytes)
	if (::send(_socket, dataSize.str().c_str(), 10, NULL) == INVALID_SOCKET)
		throw SocketException("error while sending size of data to server");

	if (::send(_socket, data.data(), data.size(), NULL) == INVALID_SOCKET)
		throw SocketException("error while sending data to server");

}

/*
	-- receive data from server --
	* output: data received to server
*/
const std::vector<char> Client::receive()
{
	
	// receive content size by receiving the first 10 bytes 
	char size[10];
	if (recv(_socket, size, 10, NULL) == INVALID_SOCKET)
		throw SocketException("error while receiving data's length");
	

	std::vector<char> buffer;
	buffer.resize(atoi(size) + 1);
	if (recv(_socket, buffer.data(), buffer.size(), NULL) == INVALID_SOCKET)
		throw SocketException("error while receiving data");
	
	return buffer;

}

/* -- creates the socket -- */
void Client::createSocket()
{
	if ((_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		throw SocketException("error invalid socket");
}

/* -- connect to server -- */
void Client::connectServer()
{

	struct sockaddr_in server;
	InetPton(AF_INET, IP, &server.sin_addr.s_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

	if (connect(_socket, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR)
		throw SocketException("cannot connect to server");

}


