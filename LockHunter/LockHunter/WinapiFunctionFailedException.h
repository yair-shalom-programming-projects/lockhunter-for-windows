#pragma once
#include <exception>
#include <string>
#include <errhandlingapi.h>

class WinapiFunctionFailedException : public std::exception
{
public:

	explicit WinapiFunctionFailedException(const std::string& funcName) :
		std::exception(("call to " + funcName + "() failed, (E = " + std::to_string(GetLastError()) + ')').c_str()) {}
};