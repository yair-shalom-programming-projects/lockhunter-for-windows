#include "RestartManager.h"

RestartManager::RestartManager() :
    _session(0), _sessionKey()
{
	if (RmStartSession(&_session, 0, _sessionKey))
		throw std::exception();
}

RestartManager::~RestartManager() noexcept
{
	RmEndSession(_session);
}


/*
    -- find all processes locking the given file --
    * input: file's path, locking processes container 
*/
void RestartManager::findProcessesLockingFile(const std::wstring& filePath, std::set<Process>& processes)
{

    // RmRegisterResources takes LPCWSTR as path which isn't const. to not change filePath arg, we copy his value
    LPCWSTR path = filePath.data();
    if (RmRegisterResources(_session, 1, &path, 0, NULL, 0, NULL) != ERROR_SUCCESS)
        throw std::exception();

    DWORD dwReason;
    UINT nProcInfoNeeded;
    UINT processesCount = 50;
    RM_PROCESS_INFO lockingProcesses[50];

    if (RmGetList(_session, &nProcInfoNeeded, &processesCount, lockingProcesses, &dwReason) != ERROR_SUCCESS)
        throw std::exception();


    for (UINT i = 0; i < processesCount; ++i)
    {
        try {
            processes.insert({ lockingProcesses[i] });
        }
        catch (...) {
        }
    }


}
