#include "Option.h"
#include <string>
#include <exception>

std::istream& operator>>(std::istream& is, Option& option)
{
	std::string input;

	if (!(is >> input))
		option = Option::INVALID;

	Option currentOption = static_cast<Option>(atoi(input.c_str()));

	if (currentOption >= Option::DELETE_FILE && currentOption <= Option::EXIT)
		option = currentOption;
	else
		option = Option::INVALID;

	return is;
}