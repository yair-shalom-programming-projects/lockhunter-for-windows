#include "WSAInitializer.h"
#include "SocketException.h"

WSAInitializer::WSAInitializer()
{
	// initialize socket library
	WSADATA wsa = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa))
		throw SocketException("error while initialize socket library");
}

WSAInitializer::~WSAInitializer()
{
	try {
		WSACleanup();
	} catch (...) {
	}
}
