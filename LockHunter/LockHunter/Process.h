﻿#pragma once
#include <unordered_map>
#include <windows.h>
#include <RestartManager.h>
#include <string>


using u64 = unsigned long long;

class Process final
{

public:

	// Constructors
	Process(const RM_PROCESS_INFO& processInfo);
	~Process();

	// Operator Overloads
	friend std::wostream& operator<<(std::wostream& os, const Process& other);
	Process& operator=(const Process& other);
	bool operator<(const Process& other) const { return _id < other.getID(); };

	// Getters
	std::wstring getName() const noexcept { return _name; };
	DWORD getID() const noexcept { return _id; };
	std::wstring getPath() const noexcept { return _path; };

private:

	// Utils
	struct TimesInLands
	{
		u64 kernelSpaceTime, userSpaceTime;
	};

	const std::unordered_map<RM_APP_TYPE, std::wstring> TYPES = 
	{
		{ RmUnknownApp,	 L"Unknown"					},
		{ RmMainWindow,  L"Main Window"				},
		{ RmOtherWindow, L"Other Window"			},
		{ RmService,	 L"Windows Service"			},
		{ RmExplorer,	 L"Windows Explorer"		},
		{ RmConsole,	 L"Console Application"     },
		{ RmCritical,	 L"Critical System Process" }
	};

		
	// Methods
	TimesInLands getTimeInLands() const;
	void findParentID();
	void findCreationDate(const RM_PROCESS_INFO& info);
	void findPath();

	// Fields
	HANDLE _handle;

	DWORD _id;
	DWORD _parentID;
	DWORD _VTL;
	
	std::wstring _creationDate;
	std::wstring _name;
	std::wstring _type;
	std::wstring _path;
	

};

