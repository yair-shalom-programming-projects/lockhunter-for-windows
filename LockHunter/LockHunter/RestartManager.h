#pragma once
#include "Process.h"
#include <set>


class RestartManager final
{

public:

	// Constructors
	RestartManager();
	~RestartManager() noexcept;
	
	// Methods
	void findProcessesLockingFile(const std::wstring& filePath, std::set<Process>& container);

private:

	// Fields 
	DWORD _session;
	WCHAR _sessionKey[CCH_RM_SESSION_KEY + 1];

};

