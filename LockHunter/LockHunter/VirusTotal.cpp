#include "VirusTotal.h"
#include "SocketException.h"
#include <fstream>

using std::wstring;



//? Constructors ?//

/* -- c'tor -- */		
VirusTotal::VirusTotal() :
	_client(L"127.0.0.1", 42069)
{
}



//? Methods ?//

/*
	-- scan a file for viruses --
	* input: file's path to scan
	* output: scan results
*/
const ScanResult VirusTotal::scanFile(const wstring& filePath)
{

	// send file data
	std::vector<char> fileData;
	fileToBuffer(fileData, filePath);
	_client.send(fileData);

	// wait for scan result
	std::string result = _client.receive().data();
	
	return {
		result.find("error") == std::string::npos,
		result
	};

}

/*
	-- convering to a file to a string buffer --
	* input: string file container, file's path
*/
void VirusTotal::fileToBuffer(std::vector<char>& container, const wstring& filePath)
{

	std::ifstream file(filePath, std::ios::binary);
	
	// unset flag skipping white spaces
	file.unsetf(std::ios::skipws);
	
	std::copy(std::istream_iterator<char>(file),
		std::istream_iterator<char>(),
		std::back_inserter(container));

}
