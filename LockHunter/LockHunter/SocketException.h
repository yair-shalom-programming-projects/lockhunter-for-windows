#pragma once
#include <exception>
#include <string>

class SocketException : public std::exception
{
public:
	explicit SocketException(const std::string_view& e) : std::exception(e.data()) {}
};