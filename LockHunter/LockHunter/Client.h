#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <vector>

class Client final
{

public:

	// Constructors
	Client(const std::wstring_view ip, const u_short& port);
	~Client() noexcept;

	// Methods
	void send(const std::vector<char>& data);
	const std::vector<char> receive();
	
private:

	// Methods
	void createSocket();
	void connectServer();
	 
	// Fields
	const wchar_t* IP;
	const u_short PORT;

	SOCKET _socket;
};

