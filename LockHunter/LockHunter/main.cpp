#pragma comment(lib, "ws2_32.lib")
#include "WSAInitializer.h"
#include "LockHunter.h"
#include <iostream>



using std::cout, std::endl, std::wcout, std::wcin;


int wmain(int argc, WCHAR** argv)
{

    std::wstring filePath = L"";

    if (argc > 1)
    {
        filePath = argv[1];
    }
    else
    {
        cout << "Please enter a locked file/folder: " << endl;
        wcin >> filePath;
    }
    
    try 
    {
        wcout << '"' << filePath << '"' << endl;
        WSAInitializer wsaInit;
        LockHunter(filePath).menu();
    }
    catch (const std::exception& e)
    {
        cout << "Error: " << e.what() << endl;
    }

    return 0;
}								