#pragma once
#include <set>
#include "Process.h"		
#include "Option.h"		


class LockHunter final
{
public:

	// Constructors
	LockHunter(const std::wstring& filePath);
	void menu();

private:

	// Methods
	void findProcessesLockingFile();

	// Menu Methods
	bool deleteFile();
	bool unlockFile();
	bool printProcesses() const;
	bool scanForVirus() const;

	// Fields
	std::set<Process> _processes;
	LPCWSTR _filePath;
};
