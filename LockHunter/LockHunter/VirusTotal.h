#pragma once
#include "Client.h"
#include <vector>
#include <string>




struct ScanResult
{
	bool success;
	std::string data;
};


class VirusTotal final
{

public:
	
	// Constructors
	explicit VirusTotal();

	// Methods
	const ScanResult scanFile(const std::wstring& filePath);


private:
	
	// Methods
	void fileToBuffer(std::vector<char>& buffer, const std::wstring& filePath);
	
	// Fields
	Client _client;
};
