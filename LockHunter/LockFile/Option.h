#pragma once
#include <iostream>
#include <string>
#include <exception>

enum class Option
{
	INVALID,
	DELETE_FILE,
	UNLOCK_FILE, 
	SHOW_LOCKING_PROCS_INFO, 
	EXIT
};

std::istream& operator>>(std::istream& is, Option& option)
{

    std::string input;

    if (!(is >> input))
           throw std::exception("invalid input");

	Option currentOption = static_cast<Option>(atoi(input.c_str()));
	
	if (currentOption >= Option::DELETE_FILE && currentOption <= Option::EXIT)
		option = currentOption;
	else
		option = Option::INVALID;

	return is;
}